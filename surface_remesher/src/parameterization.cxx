/*
 * parameterization.cxx
 *
 *  Created on: Dec 15, 2016
 *      Author: shayan
 *
 * id:
 * http://cgal-discuss.949826.n4.nabble.com/Getting-facet-indexes-from-polyhedron-3-td4553195.html
 * builder:
 * cgal-discuss.949826.n4.nabble.com/Getting-facet-indexes-from-polyhedron-3-td4553195.html
 *
 *Updated in 2019 for the new CGAL API.
 */

#define WITH_CGAL

// I also tried using this code, but it was not sucessful. It created zero area triangles.
#undef WITH_YOSHIZAWA

#include <algorithm>
#include <cmath>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <sstream>

#include "GetPot"
#include "armadillo"
#include "mesh_io.hxx"
#include "parameterization.hxx"
#include "pointer_mesh.hxx"

#ifdef WITH_CGAL
// Old parameterization API (4.10 and older) -- does not exist anymore.
// #include <CGAL/Parameterization_polyhedron_adaptor_3.h>
// #include <CGAL/parameterize.h>
// I also won't use polyhderon. I could not figure out how to use it.
// #include <CGAL/Simple_cartesian.h>
// #include <CGAL/Polyhedron_3.h>
// #include <CGAL/Polyhedron_items_with_id_3.h>
// #include <CGAL/IO/Polyhedron_iostream.h>
// #include <CGAL/Polyhedron_incremental_builder_3.h>
// New  parameterization API (master on may 7 2019)
#include <CGAL/Polygon_mesh_processing/measure.h>
#include <CGAL/Simple_cartesian.h>
#include <CGAL/Surface_mesh.h>
#include <CGAL/Surface_mesh_parameterization/parameterize.h>
#include <CGAL/Unique_hash_map.h>
#include <CGAL/boost/graph/properties_Polyhedron_3.h>
#endif

#ifdef WITH_YOSHIZAWA
#include "../yoshizawa/IDList.h"
#include "../yoshizawa/IDSet.h"
#include "../yoshizawa/PCBCGSolver.h"
#include "../yoshizawa/Point2d.h"
#include "../yoshizawa/Point3d.h"
#include "../yoshizawa/PointTool.h"
#include "../yoshizawa/PolarList.h"
#include "../yoshizawa/Polyhedron.h"
#endif

namespace
{

#ifdef WITH_CGAL

#if 0 // With polyhedron
typedef CGAL::Simple_cartesian<double> CGAL_Kernel;
//typedef CGAL::Polyhedron_3<CGAL_Kernel, CGAL::Polyhedron_items_with_id_3> CGAL_Polyhedron;
//typedef CGAL_Polyhedron::HalfedgeDS CGAL_HalfedgeDS;

// A modifier converting a Mesh mesh to CGAL Polyhedron_3
template<class HDS>
class CGAL_Mesh2Poly_Object : public CGAL::Modifier_base<HDS>
{
public:
  CGAL_Mesh2Poly_Object(Mesh & mesh)
  : m_mesh(mesh)
  {
  }

  void operator()(HDS & hds)
  {

    // Postcondition: `hds' is a valid polyhedral surface.
    CGAL::Polyhedron_incremental_builder_3<HDS> B(hds, true);

    // begin surface
    B.begin_surface(m_mesh.verts.n_mems(), m_mesh.faces.n_mems());

    // vertices
    typedef typename HDS::Vertex::Point CGAL_Vertex;
    for(int i = 0; i < m_mesh.verts.n_mems(); i++)
    {
      Vertex * vert = m_mesh.verts.get_member_ptr(i);
      typename HDS::Vertex_handle vh = B.add_vertex(CGAL_Vertex(vert->xyz(0), vert->xyz(1), vert->xyz(2)));
      vh->id() = i;
    }

    // triangles
    for(int i = 0; i < m_mesh.faces.n_mems(); i++)
    {
      Face * face = m_mesh.faces.get_member_ptr(i);
      typename HDS::Face_handle fh = B.begin_facet();
      B.add_vertex_to_facet(face->he->vert->idx());
      B.add_vertex_to_facet(face->he->next->vert->idx());
      B.add_vertex_to_facet(face->he->next->next->vert->idx());
      B.end_facet();
      fh->id() = i;
    }

    // end surface
    B.end_surface();
  }

private:
  Mesh & m_mesh;
};


void
CGAL_mesh2polyhedron(Mesh & mesh, CGAL_Polyhedron & P)
{
  CGAL_Mesh2Poly_Object<CGAL_HalfedgeDS> builder(mesh);
  P.delegate(builder);
}

void
CGAL_polyhedron2mesh(CGAL_Polyhedron & P, Mesh & mesh)
{
  std::vector<double> coords;
  std::vector<int> conn;

  for(CGAL_Polyhedron::Vertex_iterator it = P.vertices_begin(); it != P.vertices_end(); ++it)
  {
    coords.push_back(it->point().x());
    coords.push_back(it->point().y());
    coords.push_back(it->point().z());
  }

  for(CGAL_Polyhedron::Face_iterator it = P.facets_begin(); it != P.facets_end(); ++it)
  {
    CGAL_Polyhedron::Halfedge_around_facet_circulator circ = it->facet_begin();
    do
    {
      conn.push_back(int(circ->vertex()->id()));
    } while(++circ != it->facet_begin());
  }

  mesh.init(coords, conn);
}

bool
CGAL_parametrize(Mesh & mesh, CGAL_Polyhedron & sm, std::string & errmsg)
{

  namespace SMP = CGAL::Surface_mesh_parameterization;

  typedef boost::graph_traits<CGAL_Polyhedron>::halfedge_descriptor Halfedge_descriptor;
  typedef CGAL::Unique_hash_map<Halfedge_descriptor, CGAL_Kernel::Point_2> UV_uique_hash_map;
  typedef boost::associative_property_map<UV_uique_hash_map> UV_property_map;

  // adaptor
  // a halfedge on the border
  Halfedge_descriptor bhd = CGAL::Polygon_mesh_processing::longest_border(sm).first;
  UV_uique_hash_map uv_uhm;
  UV_property_map uv_pm(uv_uhm);

  // The UV property map that holds the parameterized values
  SMP::parameterize(mesh, bhd, uv_pm);

  // Set U and V
  CGAL_Polyhedron::Vertex_const_iterator pVertex;
  for(pVertex = sm.vertices_begin(); pVertex != sm.vertices_end(); pVertex++)
  {
    CGAL_Polyhedron::Halfedge_const_handle he_handle = pVertex->halfedge();

    // (u,v) pair is stored in any halfedge
    const double u = uv_pm[he_handle].x();
    const double v = uv_pm[he_handle].y();
    Vertex * vert = mesh.verts.get_member_ptr(int(pVertex->id()));
    vert->xyz(0) = u;
    vert->xyz(1) = v;
    vert->xyz(2) = 0;
  }

  return true;
} // End CGAL_parametrize



int
CGAL_map_test(int argc, char * argv[])
{
  GetPot getpot(argc, argv);
  Mesh mesh, m2;
  MeshIO io(mesh), io2(m2);
  AdaptiveRemesher arm(mesh, getpot);
  Mesh::ring_iterator it;
  std::stringstream ss;
  PatchProjector proj(mesh);

  // Read the mesh
  std::string iname = getpot.follow("meshes/square.off", "-i");
  io.read_auto(iname);

  // Create a CGAL mesh
  CGAL_Polyhedron P;
  CGAL_mesh2polyhedron(mesh, P);

  // Create the second mesh
  CGAL_polyhedron2mesh(P, m2);
  io2.write_vtk("cgal2.vtk");

  // test
  for(int i = 0; i < m2.faces.n_mems(); i++)
  {
    Face * f0 = mesh.faces.get_member_ptr(i);
    Face * f1 = m2.faces.get_member_ptr(i);
    Vertex * fv0[3] = {f0->he->vert, f0->he->next->vert, f0->he->next->next->vert};
    Vertex * fv1[3] = {f0->he->vert, f1->he->next->vert, f1->he->next->next->vert};

    if(i == 0)
    {
      std::cout << fv0[0]->idx() << " " << fv0[1]->idx() << " " << fv0[2]->idx() << std::endl;
      std::cout << fv1[0]->idx() << " " << fv1[1]->idx() << " " << fv1[2]->idx() << std::endl;
    }

    assert(fv0[0]->idx() == fv1[0]->idx());
    assert(fv0[1]->idx() == fv1[1]->idx());
    assert(fv0[2]->idx() == fv1[2]->idx());
  }

  std::string msg;
  bool scss;
  scss = CGAL_parametrize(m2, P, msg);

  if(!scss)
  {
    std::cout << "[Error] parametrizer: " << msg << std::endl;
  }

  io2.write_vtk("cgal3.vtk");

  return 0;
}
#endif // use polyhedron


typedef CGAL::Simple_cartesian<double> CGAL_Kernel;
typedef CGAL::Surface_mesh<CGAL_Kernel::Point_3> CGAL_SurfaceMesh;

void
CGAL_mesh2cgalsurfacemesh(Mesh & mesh, CGAL_SurfaceMesh & cgal_mesh)
{
  // https://stackoverflow.com/questions/45652756/cgal-surface-mesh-connectivity

  // vertices
  for(int i = 0; i < mesh.verts.n_mems(); i++)
  {
    Vertex * vert = mesh.verts.get_member_ptr(i);
    CGAL_SurfaceMesh::Vertex_index vh =
        cgal_mesh.add_vertex(CGAL_Kernel::Point_3(vert->xyz(0), vert->xyz(1), vert->xyz(2)));
    assert(i == (int)vh);
  }

  // triangles
  for(int i = 0; i < mesh.faces.n_mems(); i++)
  {
    Face * face = mesh.faces.get_member_ptr(i);
    CGAL_SurfaceMesh::Vertex_index v0(face->he->vert->idx());
    CGAL_SurfaceMesh::Vertex_index v1(face->he->next->vert->idx());
    CGAL_SurfaceMesh::Vertex_index v2(face->he->next->next->vert->idx());
    CGAL_SurfaceMesh::Face_index fh = cgal_mesh.add_face(v0, v1, v2);
    assert(i == (int)fh);
  }
}

void
CGAL_cgalsurfacemesh2mesh(CGAL_SurfaceMesh & cgal_mesh, Mesh & mesh)
{
  std::vector<double> coords;
  std::vector<int> conn;

  for(CGAL_SurfaceMesh::Vertex_iterator it = cgal_mesh.vertices_begin(); it != cgal_mesh.vertices_end(); ++it)
  {
    coords.push_back(cgal_mesh.point(*it).x());
    coords.push_back(cgal_mesh.point(*it).y());
    coords.push_back(cgal_mesh.point(*it).z());
  }

  for(CGAL_SurfaceMesh::Face_iterator it = cgal_mesh.faces_begin(); it != cgal_mesh.faces_end(); ++it)
  {
    CGAL_SurfaceMesh::Halfedge_index he = cgal_mesh.halfedge(*it);
    CGAL_SurfaceMesh::Vertex_around_face_range iter_range = cgal_mesh.vertices_around_face(he);
    for(CGAL_SurfaceMesh::Vertex_around_face_iterator iter = iter_range.begin(); iter != iter_range.end(); ++iter)
    {
      conn.push_back(int(*iter));
    }
  }

  mesh.init(coords, conn);
}


bool
CGAL_parametrize(Mesh & mesh, CGAL_SurfaceMesh & sm, std::string & errmsg)
{

  namespace SMP = CGAL::Surface_mesh_parameterization;

  typedef boost::graph_traits<CGAL_SurfaceMesh>::vertex_descriptor Vertex_descriptor;
  typedef boost::graph_traits<CGAL_SurfaceMesh>::halfedge_descriptor Halfedge_descriptor;

  // adaptor
  // a halfedge on the border
  Halfedge_descriptor bhd = CGAL::Polygon_mesh_processing::longest_border(sm).first;
  using UV_pmap = CGAL_SurfaceMesh::Property_map<Vertex_descriptor, CGAL_Kernel::Point_2>;
  UV_pmap uv_map = sm.add_property_map<Vertex_descriptor, CGAL_Kernel::Point_2>("h:uv").first;

  // The UV property map that holds the parameterized values
  SMP::parameterize(sm, bhd, uv_map);

  // Set U and V
  CGAL_SurfaceMesh::Vertex_iterator pVertex;
  for(pVertex = sm.vertices_begin(); pVertex != sm.vertices_end(); ++pVertex)
  {
    // (u,v) pair is stored in any halfedge
    const double u = uv_map[*pVertex].x();
    const double v = uv_map[*pVertex].y();
    Vertex * vert = mesh.verts.get_member_ptr(int(*pVertex));
    vert->xyz(0) = u;
    vert->xyz(1) = v;
    vert->xyz(2) = 0;
  }

  return true;
} // End CGAL_parametrize

#endif // WITH_CGAL



bool
parameterize_CGAL(Mesh & mesh, std::string & errmsg)
{
#ifdef WITH_CGAL

#if 0 // use polyhedron
  // Create a CGAL mesh
  CGAL_Polyhedron P;
  CGAL_mesh2polyhedron(mesh, P);

  // Parametrize
  bool scss;
  scss = CGAL_parametrize(mesh, P, errmsg);

  return scss;

  assert(0);
#endif // use polyhedron

  // Create a CGAL mesh

  CGAL_SurfaceMesh cgal_mesh;
  CGAL_mesh2cgalsurfacemesh(mesh, cgal_mesh);

  // Parametrize
  bool scss;
  scss = CGAL_parametrize(mesh, cgal_mesh, errmsg);

  return scss;

  assert(0);

#else // WITH_CGAL

  errmsg = "CGAL NOT LINKED";
  return false;
#endif
}

bool
parameterize_yoshizawa(Mesh & mesh, std::string & errmsg)
{
#ifdef WITH_YOSHIZAWA
  std::vector<double> pos;
  std::vector<int> conn;

  pos.reserve(mesh.verts.n_mems() * 3);
  conn.reserve(mesh.faces.n_mems() * 3);

  for(int i = 0; i < mesh.verts.n_mems(); i++)
  {
    Vertex * vert = mesh.verts.get_member_ptr(i);
    pos.push_back(vert->xyz(0));
    pos.push_back(vert->xyz(1));
    pos.push_back(vert->xyz(2));
  }
  for(int i = 0; i < mesh.faces.n_mems(); i++)
  {
    Face * face = mesh.faces.get_member_ptr(i);
    conn.push_back(face->he->vert->idx());
    conn.push_back(face->he->next->vert->idx());
    conn.push_back(face->he->next->next->vert->idx());
  }

  Polyhedron yoshizawa_mesh;
  yoshizawa_mesh.readmesh((int)pos.size() / 3, pos.data(), (int)conn.size() / 3, conn.data());
  yoshizawa_mesh.param();
  yoshizawa_mesh.writemesh(pos, conn);

  for(int i = 0; i < mesh.verts.n_mems(); i++)
  {
    Vertex * vert = mesh.verts.get_member_ptr(i);
    vert->xyz(0) = pos[3 * i + 0];
    vert->xyz(1) = pos[3 * i + 1];
    vert->xyz(2) = pos[3 * i + 2];
  }

  errmsg = "";
  return true;
}
#else // WITH_YOSHIZAWA
  errmsg = "YOSHIZAWA NOT LINKED";
  return false;
#endif
}


} // namespace

bool
parameterize(Mesh & mesh, std::string & errmsg, const ParameterizationType type)
{
  switch(type)
  {
  case PARAMETERIZATION_YOSHIZAWA: return parameterize_yoshizawa(mesh, errmsg);
  case PARAMETERIZATION_CGAL: return parameterize_CGAL(mesh, errmsg);
  }
  assert(0);
  return false;
}
