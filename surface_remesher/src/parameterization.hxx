/*
 * cgal_tools.hxx
 *
 *  Created on: Dec 15, 2016
 *      Author: hooshi
 */

#ifndef INCLUDE_CGAL_TOOLS_HXX_
#define INCLUDE_CGAL_TOOLS_HXX_


#include "pointer_mesh.hxx"

enum ParameterizationType
  {
    PARAMETERIZATION_CGAL = 0,
    PARAMETERIZATION_YOSHIZAWA
  };
  
bool parameterize( Mesh& , std::string&, const ParameterizationType);

#endif /* INCLUDE_CGAL_TOOLS_HXX_ */
